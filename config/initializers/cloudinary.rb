Cloudinary.config do |config|
  config.cloud_name = ENV['CLOUDINARY_BUCKET_NAME'] || "development"
  config.api_key = ENV['CLOUDINARY_ACCESS_KEY']
  config.api_secret = ENV['CLOUDINARY_SECRET_ACCESS_KEY']
  config.cdn_subdomain = true
  config.secure = true
  if Rails.env.production?
    config.enhance_image_tag = true
    config.static_image_support = true
  end
end

