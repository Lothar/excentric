source 'https://rubygems.org'
ruby '2.2.1' # Specify version of ruby to be used for Heroku

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.4'

# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 2.7.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring',        group: :development

gem 'haml-rails'

# Create thumbnails
gem 'mini_magick'

# Upload files
gem 'carrierwave'

# Use rspec tests
gem 'rspec-rails'

# Authentication
gem 'devise'

# Add internationalisation
gem 'rails-i18n', '~> 4.0.0'

# Internationalisation for devise
gem 'devise-i18n'

# Send & view emails in development
gem "letter_opener", group: :development

# Handle forms with simple form
gem 'simple_form'

# Datetime picker for datetime form fields
gem 'jquery-datetimepicker-rails'

# Make friendly urls
gem "friendly_id"

# Rails asset-pipeline gem to simply add/remove nested objects
gem 'cocoon'

# Add a calendar to handle events
gem 'clndr-rails', github: 'sedx/clndr-rails'

# Use Cloudinary service for assets storage (must be loaded after carrierwave or paperclip gems)
gem 'cloudinary'

# Add puma webserver to handle multiple requests
gem "puma"

# New Relic gem to keep Heroku dyno alive and get requests information
gem 'newrelic_rpm'

# Add super for Google map
gem 'gmaps4rails'

# Add console support
gem 'web-console', '~> 2.0', group: :development

# Assets support for heroku
gem 'rails_12factor', group: :production

# # Handle 404 and 500 error exceptions
# gem 'exception_handler'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use byebug to debug
gem 'byebug', group: [:development, :test]

gem 'dalli'
gem 'memcachier'
