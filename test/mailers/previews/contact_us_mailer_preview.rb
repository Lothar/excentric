class ContactUsMailerPreview < ActionMailer::Preview
  def contact_us_email_preview
    ContactUsMailer.contact_us_email(ContactUs.first)
  end
end
