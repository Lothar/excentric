require 'rails_helper'

RSpec.describe "partners/new", type: :view do
  before(:each) do
    assign(:partner, Partner.new(
      :website => "MyString",
      :description => "MyText",
      :image => "MyString"
    ))
  end

  it "renders new partner form" do
    render

    assert_select "form[action=?][method=?]", partners_path, "post" do

      assert_select "input#partner_website[name=?]", "partner[website]"

      assert_select "textarea#partner_description[name=?]", "partner[description]"

      assert_select "input#partner_image[name=?]", "partner[image]"
    end
  end
end
