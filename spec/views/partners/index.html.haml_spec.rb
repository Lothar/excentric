require 'rails_helper'

RSpec.describe "partners/index", type: :view do
  before(:each) do
    assign(:partners, [
      Partner.create!(
        :website => "Website",
        :description => "MyText",
        :image => "Image"
      ),
      Partner.create!(
        :website => "Website",
        :description => "MyText",
        :image => "Image"
      )
    ])
  end

  it "renders a list of partners" do
    render
    assert_select "tr>td", :text => "Website".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Image".to_s, :count => 2
  end
end
