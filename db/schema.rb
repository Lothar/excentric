# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160229140211) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "artistes", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "last_name",           limit: 255
    t.string   "first_name",          limit: 255
    t.string   "alias",               limit: 255
    t.text     "slideshow_biography"
    t.string   "slideshow_image",     limit: 255
    t.text     "biography"
    t.boolean  "on_display"
    t.boolean  "to_display"
    t.string   "presentation_image",  limit: 255
    t.string   "slug",                limit: 255
    t.integer  "slideshow_position"
    t.text     "next_exhibition"
  end

  add_index "artistes", ["slideshow_position"], name: "index_artistes_on_slideshow_position", using: :btree

  create_table "contact_us", force: :cascade do |t|
    t.string   "last_name",  limit: 255
    t.string   "first_name", limit: 255
    t.string   "email",      limit: 255
    t.string   "object",     limit: 255
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "last_name",  limit: 255
    t.string   "first_name", limit: 255
    t.string   "email",      limit: 255
    t.string   "object",     limit: 255
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: :cascade do |t|
    t.string   "title",                 limit: 255
    t.text     "short_description"
    t.text     "description"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "to_display"
    t.string   "slideshow_image",       limit: 255
    t.text     "slideshow_description"
    t.boolean  "on_display"
    t.string   "type_event",            limit: 255
    t.integer  "slideshow_position"
  end

  add_index "events", ["end_date"], name: "index_events_on_end_date", using: :btree
  add_index "events", ["slideshow_position"], name: "index_events_on_slideshow_position", using: :btree
  add_index "events", ["start_date"], name: "index_events_on_start_date", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 255, null: false
    t.integer  "sluggable_id",               null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 255
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "partners", force: :cascade do |t|
    t.string   "website",     limit: 255
    t.text     "description"
    t.string   "image",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "to_display"
    t.string   "name"
  end

  create_table "press_articles", force: :cascade do |t|
    t.string   "title",           limit: 255
    t.string   "photo",           limit: 255
    t.text     "short_paragraph"
    t.text     "paragraph"
    t.boolean  "to_display"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug",            limit: 255
    t.datetime "published_on"
    t.string   "from_website",    limit: 255
    t.string   "full_link",       limit: 255
  end

  add_index "press_articles", ["created_at"], name: "index_press_articles_on_created_at", using: :btree
  add_index "press_articles", ["slug"], name: "index_press_articles_on_slug", using: :btree

  create_table "sounds", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.string   "file",       limit: 255
    t.integer  "artiste_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sounds", ["artiste_id"], name: "index_sounds_on_artiste_id", using: :btree

  create_table "works", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.string   "type_file",  limit: 255
    t.string   "file",       limit: 255
    t.integer  "artiste_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "works", ["artiste_id"], name: "index_works_on_artiste_id", using: :btree

end
