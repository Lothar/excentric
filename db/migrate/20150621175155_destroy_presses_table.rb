class DestroyPressesTable < ActiveRecord::Migration
  def change
    drop_table :presses
  end
end
