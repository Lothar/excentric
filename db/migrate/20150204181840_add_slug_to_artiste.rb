class AddSlugToArtiste < ActiveRecord::Migration
  def change
    add_column :artistes, :slug, :string
  end
end
