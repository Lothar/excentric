class AddIndexesToTables < ActiveRecord::Migration
  def change
    add_index :artistes, :slideshow_position
    add_index :events, :start_date
    add_index :events, :end_date
    add_index :events, :slideshow_position
    add_index :press_articles, :created_at
    add_index :sounds, :artiste_id
    add_index :works, :artiste_id
  end
end
