class AddToDisplayToEvent < ActiveRecord::Migration
  def change
    add_column :events, :to_display, :boolean
  end
end
