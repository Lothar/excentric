class CreateWorks < ActiveRecord::Migration
  def change
    create_table :works do |t|
      t.string :title
      t.string :type_file
      t.string :file
      t.integer :artiste_id

      t.timestamps
    end
  end
end
