class AddFieldsToArtiste < ActiveRecord::Migration
  def change
    add_column :artistes, :last_name, :string
    add_column :artistes, :first_name, :string
    add_column :artistes, :alias, :string
    add_column :artistes, :slideshow_biography, :text
    add_column :artistes, :slideshow_image, :string
    add_column :artistes, :biography, :text
  end
end
