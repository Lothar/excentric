class AddFieldsToEvent < ActiveRecord::Migration
  def change
    add_column :events, :slideshow_description, :string
    add_column :events, :on_display, :boolean
  end
end
