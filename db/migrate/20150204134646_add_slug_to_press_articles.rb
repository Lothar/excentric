class AddSlugToPressArticles < ActiveRecord::Migration
  def change
    add_column :press_articles, :slug, :string
    add_index :press_articles, :slug
  end
end
