class AddNextExhibitionToArtiste < ActiveRecord::Migration
  def change
    add_column :artistes, :next_exhibition, :text
  end
end
