class ChangeSlideshowDescriptionFormatInEvents < ActiveRecord::Migration
  def change
    change_column :events, :slideshow_description, :text
  end
end
