class AddSlideshowPositionToArtiste < ActiveRecord::Migration
  def change
    add_column :artistes, :slideshow_position, :integer
  end
end
