class AddPresentationImageToArtiste < ActiveRecord::Migration
  def change
    add_column :artistes, :presentation_image, :string
  end
end
