class RenameShortDescriptFromEvent < ActiveRecord::Migration
  def change
    rename_column :events, :short_descript, :short_description
  end
end
