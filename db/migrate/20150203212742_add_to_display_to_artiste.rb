class AddToDisplayToArtiste < ActiveRecord::Migration
  def change
    add_column :artistes, :to_display, :boolean
  end
end
