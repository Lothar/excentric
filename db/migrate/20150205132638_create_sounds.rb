class CreateSounds < ActiveRecord::Migration
  def change
    create_table :sounds do |t|
      t.string :title
      t.string :file
      t.integer :artiste_id

      t.timestamps
    end
  end
end
