class AddFieldsToPressArticle < ActiveRecord::Migration
  def change
    add_column :press_articles, :published_on, :datetime
    add_column :press_articles, :from_website, :string
    add_column :press_articles, :full_link, :string
  end
end
