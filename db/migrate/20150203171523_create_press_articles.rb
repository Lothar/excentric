class CreatePressArticles < ActiveRecord::Migration
  def change
    create_table :press_articles do |t|
      t.string :title
      t.string :photo
      t.text :short_paragraph
      t.text :paragraph
      t.boolean :to_display

      t.timestamps
    end
  end
end
