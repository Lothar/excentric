class AddSlideshowImageToEvent < ActiveRecord::Migration
  def change
    add_column :events, :slideshow_image, :string
  end
end
