class AddSlideshowPositionToEvent < ActiveRecord::Migration
  def change
    add_column :events, :slideshow_position, :integer
  end
end
