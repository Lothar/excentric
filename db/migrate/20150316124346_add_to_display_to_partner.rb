class AddToDisplayToPartner < ActiveRecord::Migration
  def change
    add_column :partners, :to_display, :boolean
  end
end
