class AddOnDisplayToArtiste < ActiveRecord::Migration
  def change
    add_column :artistes, :on_display, :boolean
  end
end
