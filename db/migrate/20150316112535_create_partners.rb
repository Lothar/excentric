class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :website
      t.text :description
      t.string :image

      t.timestamps
    end
  end
end
