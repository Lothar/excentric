class AdminsController < ApplicationController
  before_action :authenticate_admin!

  respond_to :html

  def index
    @admins = Admin.all.order(:email)
    @response = { admins: @admins, visited: "admin", from: "admins"}
    respond_with(@response)
  end

end
