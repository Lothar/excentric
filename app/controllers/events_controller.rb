class EventsController < ApplicationController
  before_action :authenticate_admin!, except: [:display, :show]
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  respond_to :html
  def index
    @events = Event.all.order(:start_date)
    @response = { events: @events, visited: "admin", from: "events"}
    render template: 'admins/index'
  end

  def display
    @events = Event.filter_events(params[:type], params[:in_time])
    @response = { visited: "news"}
    @event_clndr = Event.set_calendar(@events, :events_calendar)
  end

  def show
    @response = { visited: "news" }
    @event_clndr = Event.set_calendar([@event], :events_calendar)
  end

  def new
    @event = Event.new
    respond_with(@event)
  end

  def edit
  end

  def create
    @event = Event.new(event_params)
    @event.save
    respond_with(@event, location: events_path)
  end

  def update
    @event.update(event_params)
    respond_with(@event, location: events_path)
  end

  def destroy
    @event.destroy
    redirect_to action: :index
  end

  private
    def set_event
      @event = Event.find(params[:title])
    end

    def event_params
      params.require(:event).permit(:title, :slideshow_image, :short_description, :description, :start_date, :end_date, :to_display, :on_display, :slideshow_description, :type_event, :slideshow_position)
    end
end
