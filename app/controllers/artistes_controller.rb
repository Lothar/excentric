class ArtistesController < ApplicationController
  before_action :authenticate_admin!, except: [:display, :show]
  before_action :set_artiste, only: [:show, :edit, :update, :destroy]

  respond_to(:html)

  def index 
    @artistes = Artiste.includes(:works).order(:last_name)
    @response = { artistes: @artistes, visited: "admin", tab: "artistes"}
    render template: 'admins/index'
  end

  def display
    @artistes = Artiste.filter_artistes(params[:type])
    title = Artiste.display_title(params[:type])
    @response = { visited: "artistes", title: title }
  end

  def show
    @response = { visited: "artistes" }
  end

  def new
    @artiste = Artiste.new
    @work = @artiste.works.build
    respond_with(@artiste)
  end

  def edit
  end

  def create
    @artiste = Artiste.new(artiste_params)
    @artiste.save
    respond_with(@artiste, location: artistes_path)
  end

  def update
    @artiste.update(artiste_params)
    respond_with(@artiste, location: artistes_path)
  end

  def destroy
    @artiste.destroy
    redirect_to action: :index
  end

  private
    def set_artiste
      @artiste = Artiste.friendly.preload(:works).find(params[:name])
    end

    def artiste_params
      allowed_params = 
        [
          :last_name, 
          :first_name, 
          :alias, 
          :slideshow_image, 
          :slideshow_biography, 
          :biography, 
          :on_display, 
          :to_display, 
          :presentation_image, 
          :slideshow_position,
          :next_exhibition,
          works_attributes: 
          [
            :title, 
            :type_file, 
            :file, 
            :id, 
            :_destroy
          ],
          sounds_attributes:
          [
            :title,
            :file,
            :id,
            :_destroy
          ]
        ]

      params.require(:artiste).permit(allowed_params)
    end

end
