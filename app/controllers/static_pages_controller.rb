class StaticPagesController < ApplicationController

  respond_to :html

  def legal_notes
  end

  def about
    @response = {visited: "about"}
  end

  def site_map
  end

end
