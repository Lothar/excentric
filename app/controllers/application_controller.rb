class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # http_basic_authenticate_with name: "excentric", password: "excentric12345", except: :index
  
  def after_sign_in_path_for(resource)
    artistes_path
  end

end
