class ContactUsController < ApplicationController
  respond_to :html

  def new
    @contact_us = ContactUs.new

    flash[:notice] = nil unless params[:notice].present?

    @response = {contact_us: @contact_us, visited: "contact"} 
    respond_with(@response)
  end

  def create
    @contact_us = ContactUs.new(contact_us_params)

    # Sends email to excentric when a contact-us form has been submitted.
    ContactUsMailer.contact_us_email(@contact_us).deliver_later && flash[:notice] = "Votre message a bien été envoyé." if @contact_us.save

    respond_with(@contact_us, location: new_contact_us_path(notice: true))
  end

  private
    def contact_us_params
      params.require(:contact_us).permit(:last_name, :first_name, :email, :object, :message)
    end

end
