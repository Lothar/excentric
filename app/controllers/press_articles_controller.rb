class PressArticlesController < ApplicationController
  before_action :authenticate_admin!, except: [:display, :show]
  before_action :set_press_article, only: [:show, :edit, :update, :destroy]

  respond_to(:html)

  def index
    @press_articles = PressArticle.all.order(:created_at)
    @response = { visited: "admin", from: "press_articles"}
    render template: 'admins/index'
  end

  def display
    @press_articles = PressArticle.where(to_display: true).order(:created_at)
    @response = { visited: "news"}
  end

  def show
    @response = { visited: "news"}
  end

  def new
    @press_article = PressArticle.new
    respond_with(@press_article)
  end

  def edit
  end

  def create
    @press_article = PressArticle.new(press_article_params)
    @press_article.save
    respond_with(@press_article, location: press_articles_path)
  end

  def update
    @press_article.update(press_article_params)
    respond_with(@press_article, location: press_articles_path)
  end

  def destroy
    @press_article.destroy
    redirect_to action: :index
  end

  private
    def set_press_article
      @press_article = PressArticle.friendly.find(params[:title])
    end

    def press_article_params
      params.require(:press_article).permit(:title, :photo, :short_paragraph, :paragraph, :to_display, :published_on, :from_website, :full_link)
    end

end
