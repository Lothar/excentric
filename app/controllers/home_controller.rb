class HomeController < ApplicationController

  respond_to :html

  def index
    @slideshow_collection = Home.slideshow_collection 
    @response = { visited: "home"}
    respond_with(@response)
  end

end
