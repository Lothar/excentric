class PartnersController < ApplicationController
  before_action :authenticate_admin!, except: [:display]
  before_action :set_partner, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @partners = Partner.all
    @response = { visited: "admin", from: "partners"}
    render template: 'admins/index'
  end

  def display
    @partners = Partner.where(to_display: true)
    @response = { visited: "about" }
  end

  def new
    @partner = Partner.new
    respond_with(@partner)
  end

  def edit
  end

  def create
    @partner = Partner.new(partner_params)
    @partner.save
    respond_with(@partner, location: partners_path)
  end

  def update
    @partner.update(partner_params)
    respond_with(@partner, location: partners_path)
  end

  def destroy
    @partner.destroy
    redirect_to action: :index
  end

  private
    def set_partner
      @partner = Partner.find(params[:id])
    end

    def partner_params
      params.require(:partner).permit(:name, :website, :description, :image, :to_display)
    end
end
