class Sound < ActiveRecord::Base
  mount_uploader :file, SoundUploader

  belongs_to :artiste
  validates :title, presence: true, uniqueness: true
end
