class ContactUs < ActiveRecord::Base  
  validates :last_name, :first_name, :email, :object, :message, presence: true
  validates :email, email: { message: I18n.t('errors.messages.email') }

end
