class Artiste < ActiveRecord::Base
  extend FriendlyId
  mount_uploader :slideshow_image, SlideshowUploader
  mount_uploader :presentation_image, ArtisteUploader

  has_many :works, dependent: :destroy
  has_many :sounds, dependent: :destroy

  accepts_nested_attributes_for :works, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :sounds, reject_if: :all_blank, allow_destroy: true

  before_validation :reset_slug

  friendly_id :alias_or_full_name, use: [:slugged, :history]

  validates :last_name, :first_name, :presentation_image, presence: true
  validates :last_name, uniqueness: { scope: :first_name }
  validates :alias, uniqueness: true, allow_blank: true


  def alias_or_full_name
    if self.alias.present?
      self.alias
    else
      "#{first_name} #{last_name}"
    end 
  end

  def full_name_or_alias_changed?
    self.alias_changed? || self.first_name_changed? || self.last_name_changed?
  end


  def self.filter_artistes(type)
    if type == 'music'
      Artiste.includes(:sounds).where(to_display: true).where("sounds.id IS NOT NULL").order(:last_name).references(:sounds)
    elsif type.present?
      Artiste.includes(:works).where(to_display: true, works: {type_file: type}).order(:last_name)
    else
      Artiste.includes(:works).where(to_display: true).order(:last_name)
    end
  end

  def full_name_downcased
    "#{first_name} #{last_name}".downcase
  end

  def self.display_title(type)
    if type == "photo"
      "Photographes"
    elsif type == "sculpture"
      "Sculpteurs"
    elsif type == "music"
      "Musiciens"
    elsif type == "painting"
      "Peintres"
    else
      "Artistes"
    end
  end

  private 
    def reset_slug
      if self.full_name_or_alias_changed?
        self.slug = nil
      end
    end
end
