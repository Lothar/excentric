class Home < ActiveRecord::Base

  def self.slideshow_collection
    collection = Artiste.where(on_display: true, to_display: true).order(:slideshow_position)
    collection = collection + Event.where(on_display: true, to_display: true).where('end_date >= ?', DateTime.yesterday.end_of_day).order(:slideshow_position) 
    
    collection.sort!{|a,b| a.slideshow_position && b.slideshow_position ? a.slideshow_position <=> b.slideshow_position : a.slideshow_position ? -1 : 1}
  end

end