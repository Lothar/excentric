class Partner < ActiveRecord::Base
  mount_uploader :image, PartnerUploader

  validates :name, :website, :description, :image, presence: true
  validates :name, uniqueness: true
end
