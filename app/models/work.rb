class Work < ActiveRecord::Base
  mount_uploader :file, WorkUploader

  belongs_to :artiste
  validates :title, presence: true, uniqueness: true


  TYPE_FILE = [["Peinture","painting"],["Photographie", "photo"],["Sculpture","sculpture"]]

end
