class Event < ActiveRecord::Base
  mount_uploader :slideshow_image, EventUploader

  validates :title, :start_date, :end_date, presence: true
  validates :title, uniqueness: true
  
  def self.filter_events(type, in_time)
    if type.present?
      if in_time.present?
        Event.where(to_display: true, type_event: type).where('end_date < ?', DateTime.yesterday.end_of_day).order('start_date DESC')
      else
        Event.where(to_display: true, type_event: type).where('end_date >= ?', DateTime.yesterday.end_of_day).order(:start_date)
      end
    else
      if in_time.present?
        Event.where(to_display: true).where('end_date < ?', DateTime.yesterday.end_of_day).order('start_date DESC')
      else
        Event.where(to_display: true).where('end_date >= ?', DateTime.yesterday.end_of_day).order(:start_date)
      end
    end
  end


  def self.set_calendar(events, event_name)
    cal = Clndr.new(event_name)
    events.each do |event|
      cal.add_multiday_event(event.start_date, event.end_date, event.title)
    end
    cal
  end

end
