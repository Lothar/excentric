class PressArticle < ActiveRecord::Base
  extend FriendlyId
  before_validation :reset_slug

  friendly_id :title, use: [:slugged, :history]

  mount_uploader :photo, PressArticleUploader

  validates :title, :short_paragraph, :paragraph, presence: true
  validates :title, uniqueness: true

  private 
    def reset_slug
      if self.title_changed?
        self.slug = nil
      end
    end
end
