// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//= require jquery.datetimepicker
//= require cocoon
//= require moment
//= require moment/fr
//= require underscore
//= require clndr-rails
//= require gmaps/google

// injecting colorbox nodes when using turbolinks

// TO UNCOMMENT IF WRONG jqueyr.colorbox.min.js
// $colorboxOverlay = ''
// $colorboxBox = ''
var initialize = function() {

  // TO UNCOMMENT IF WRONG jqueyr.colorbox.min.js
  // $colorboxOverlay = $("#cboxOverlay")
  // $colorboxBox = $("#colorbox")
  // Initialize modal popup for image gallery
  jQuery('.image_gallery').colorbox({rel: 'image_gallery'});

  // Workaround bug datetimepicker jquery on arrows when there are 2 date pickers. The arrows of the first one aren't working properly
  // http://stackoverflow.com/questions/26843489/jquery-datepicker-opens-twice-after-click-on-next-month-using-2-datepickers
  jQuery("#datepickerFromDate").datetimepicker({
    onClose: function() {
        window.setTimeout(function(){
            jQuery('#datepickerToDate').datetimepicker('show');
        }, 0);
    }
  });
  jQuery("#datepickerToDate").datetimepicker();
  
  // Initialize the datetime form pickers
  $('.datetimepicker').datetimepicker({lang:'fr'});

}

jQuery(document).ready(initialize);
jQuery(document).on('page:load', initialize);

// TO UNCOMMENT IF WRONG jqueyr.colorbox.min.js (Conflict with turbolinks)
// https://github.com/jackmoore/colorbox/issues/505

// $(document).on('page:change', function() {
//   $colorboxOverlay.appendTo("body")
//   $colorboxBox.appendTo("body")
// });