class ContactUsMailer < ActionMailer::Base
  default from: "server-no-reply@excentric.com"

  def contact_us_email(contact_us)
    @contact_us = contact_us
    mail(to: "maubeuge.excentric@gmail.com", subject: "Excentric : #{@contact_us.object}")
  end

end