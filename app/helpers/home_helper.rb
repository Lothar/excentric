module HomeHelper

  def cache_key_for_slideshow_collection
    
    displayed_artists     = Artiste.where(on_display: true, to_display: true)
    count_artists         = displayed_artists.count
    max_updated_at_artist = displayed_artists.maximum(:updated_at).try(:utc).try(:to_s, :number)
    
    displayed_events      = Event.where(on_display: true, to_display: true).where('end_date >= ?', DateTime.yesterday.end_of_day)
    count_events          = displayed_events.count
    max_updated_at_event  = displayed_events.maximum(:updated_at).try(:utc).try(:to_s, :number)

    "slideshow/all-artists-#{count_artists}-#{max_updated_at_artist}-events-#{count_events}-#{max_updated_at_event}"
  end

end
