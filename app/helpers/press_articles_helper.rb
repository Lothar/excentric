module PressArticlesHelper


  def published_on_tag(published_on, from_website)
    if published_on.present? && from_website.present?
      "Publié le #{I18n.l published_on, format: :short} sur #{from_website}"
    elsif published_on.present?
      "Publié le #{I18n.l published_on, format: :shot}"
    elsif from_website.present?
      "Sur #{from_website}"
    end
  end

end
