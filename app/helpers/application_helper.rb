module ApplicationHelper

  def link_active?(visited, link)
    if visited == link
      "active"
    elsif visited == link
      "active"
    end
  end

  def render_html(text)
    Haml::Engine.new(text).render
  end

  def format_text(text)
    text.gsub(/\n/, "<br/>").html_safe
  end

  def display_filename(filename)
    filename.split("/").last.split(".")[0..-2].join(".")
  end
end
